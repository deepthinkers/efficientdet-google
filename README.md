Instruction for ThinkDeeply on Windows 10

install the desired efficientdet (0-7), tested with 0-1. 

Download the backbone and model architecture. wget is only available in PowerShell on Windows unless you install 3rd party software.
```bash
set model_idx=0
wget https://storage.googleapis.com/cloud-tpu-checkpoints/efficientdet/coco/efficientdet-d%model_idx%.tar.gz
wget https://storage.googleapis.com/cloud-tpu-checkpoints/efficientnet/ckptsaug/efficientnet-b%model_idx%.tar.gz
```

```bash
set model_idx=0
set batch_size=8
set evaluate=False
set num_epochs=100

python3 -m pip install -r requirements.txt

python main.py --mode=train      --training_file_pattern=./from_coco.tfrecord   --model_name=efficientdet-d%model_idx%      --model_dir=tmp/efficientdet-d%model_idx%-scratch --backbone_ckpt=efficientnet-b%model_idx% --train_batch_size=%batch_size%        --num_examples_per_epoch=56  --num_epochs=%num_epochs%    --hparams="image_size=400,num_classes=2,moving_average_decay=0"      --use_tpu=False --eval_batch_size=1 --eval_after_training=%evaluate%  --num_cores=1 --eval_samples=2

python model_inspect.py --runmode=saved_model    --model_name=efficientdet-d%model_idx% --ckpt_path=tmp/efficientdet-d%model_idx%-scratch    --saved_model_dir=savedmodel --hparams="moving_average_decay=0,num_classes=2"

python model_inspect.py --runmode=saved_model_video  --saved_model_dir=savedmodel  --model_name=efficientdet-d%model_idx%   --ckpt_path=tmp/efficientdet-d%model_idx%-scratch     --input_video=IR-Video-3.mp4 --output_video=our_mov.mov --hparams="moving_average_decay=0,num_classes=2,label_id_mapping=labels.yaml"
```

Errors:
WARNING:tensorflow:Reraising captured error
W0522 02:48:26.632974  4568 error_handling.py:149] Reraising captured error
Traceback (most recent call last):
  File "C:\Users\Aaron\Anaconda3\lib\site-packages\tensorflow\python\client\session.py", line 1365, in _do_call
    return fn(*args)
  File "C:\Users\Aaron\Anaconda3\lib\site-packages\tensorflow\python\client\session.py", line 1350, in _run_fn
    target_list, run_metadata)
  File "C:\Users\Aaron\Anaconda3\lib\site-packages\tensorflow\python\client\session.py", line 1443, in _call_tf_sessionrun
    run_metadata)
tensorflow.python.framework.errors_impl.InvalidArgumentError: assertion failed: [106]
         [[{{node parser/Assert/Assert}}]]
         [[IteratorGetNext]]
		 
		 
Reason:
An image in the training set has more detections than MAX_NUM_INSTANCES

Solution:
Increase MAX_NUM_INSTANCES in dataloader.py


Errors:
tensorflow.python.framework.errors_impl.NotFoundError: 2 root error(s) found.
(0) Not found: Key box_net/box-0-bn-3/beta/ExponentialMovingAverage not found in checkpoint
[[{{node save/RestoreV2}}]]
(1) Not found: Key box_net/box-0-bn-3/beta/ExponentialMovingAverage not found in checkpoint
[[{{node save/RestoreV2}}]]
[[save/RestoreV2/_149]

Solution:
set hconfig.moving_average_decay=0 in code 
or use arg --hparams="moving_average_decay=0"


Errors:
(0) Invalid argument: Assign requires shapes of both tensors to match. lhs shape= [18] rhs shape= [810]
[[node save/Assign_21 (defined at usr/local/lib/python3.6/dist-packages/tensorflow_core/python/framework/ops.py:1748) ]]
(1) Invalid argument: Assign requires shapes of both tensors to match. lhs shape= [18] rhs shape= [810]    
 [[node save/Assign_21 (defined at usr/local/lib/python3.6/dist-packages/tensorflow_core/python/framework/ops.py:1748) ]]
[[save/RestoreV2/_270]]  

Solution:
Make sure hparams num_classes is set to the correct number of classes for your data. e.g. run with --hparams="num_classes=2"